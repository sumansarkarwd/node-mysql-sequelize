module.exports = {
  alreadyExists: (entity) => `${entity} already exists`,
  invalidLoginCredentials: () => 'Invalid login credentials',
};
