const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');

const { users } = require('../models');
const ApiError = require('./apiError');
const config = require('./config');

const jwtOptions = {
  secretOrKey: config.JWT_SECRET,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtVerify = async (payload, done) => {
  try {
    if (payload.type !== 'access') throw new ApiError('Invalid token type', 'INVALID_TOKEN_TYPE');

    const user = await await users.findOne({ where: { id: payload.sub } });
    if (!user) return done(null, false);
    done(null, user);
  } catch (error) {
    done(error, false);
  }
};

const jwtStrategy = new JwtStrategy(jwtOptions, jwtVerify);

module.exports = {
  jwtStrategy,
};
