/* eslint-disable no-unused-vars */
const httpStatus = require('http-status');
const ApiError = require('../utils/apiError');
const logger = require('../utils/logger');

module.exports.errorConverter = (err, req, res, next) => {
  if (!(err instanceof ApiError)) {
    err = new ApiError(err.message);
  }

  next(err);
};

module.exports.errorHandler = (error, req, res, next) => {
  const { message, httpCode, errorCode, stack } = error;

  const resp = {
    message,
    httpCode,
    errorCode,
    stack: stack?.split('\n'),
  };

  res.status(httpCode).send(resp);

  logger.error(resp);
};

module.exports.notFoundHandler = (error, req, res) => {
  throw new ApiError('Not found', 'NOT_FOUND', null, httpStatus.NOT_FOUND);
};
