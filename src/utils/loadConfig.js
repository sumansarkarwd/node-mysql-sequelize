const Joi = require('joi');
const dotenv = require('dotenv');
const chalk = require('chalk');
const logger = require('./logger');

module.exports = () => {
  dotenv.config();

  const schema = Joi.object({
    APP_NAME: Joi.string().required(),
    NODE_ENV: Joi.string().required(),
    PORT: Joi.number().integer().positive().required(),
    API_HOST: Joi.string().required(),
    JWT_SECRET: Joi.string().required(),
    DB_HOST: Joi.string().required(),
    DB_PORT: Joi.string().required(),
    DB_USER: Joi.string().required(),
    DB_PASSWORD: Joi.string().required(),
    DB_NAME: Joi.string().required(),
  }).unknown();

  const { error } = schema.validate(process.env);

  if (error) {
    logger.error(chalk.bgRedBright.black(error.message));
    process.exit(1);
  }
};
