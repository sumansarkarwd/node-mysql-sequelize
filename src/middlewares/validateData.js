const httpStatus = require('http-status');
const Joi = require('joi');
const _ = require('lodash');
const ApiError = require('../utils/apiError');

const validate = (schema) => (req, res, next) => {
  const validSchema = _.pick(schema, ['params', 'query', 'body']);
  const object = _.pick(req, Object.keys(validSchema));

  Joi.compile(validSchema)
    .prefs({ errors: { label: 'key' } })
    .validateAsync(object, {
      errors: {
        wrap: {
          label: '',
        },
      },
    })
    .then(({ value }) => {
      Object.assign(req, value);
      return next();
    })
    .catch((error) => {
      next(new ApiError(error.message, 'VALIDATION_ERROR', null, httpStatus.BAD_REQUEST));
    });
};

module.exports = validate;
