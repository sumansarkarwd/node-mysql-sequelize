const bcrypt = require('bcrypt');
const AlreadyExistsError = require('../errors/alreadyExists.error');
const UnauthorizedError = require('../errors/unauthorized.error');
const { users } = require('../models');
const config = require('../utils/config');

module.exports.register = async (data) => {
  const userExists = await users.findOne({ where: { email: data.email } });
  if (userExists) throw new AlreadyExistsError('User');

  data.password = await bcrypt.hash(data.password, config.SALT_ROUNDS);
  const user = await users.create(data);

  return user.toJSON();
};

module.exports.login = async (data) => {
  const userExists = await users.findOne({ where: { email: data.email } });
  if (!userExists) throw new UnauthorizedError();

  const isValid = await bcrypt.compare(data.password, userExists.password);
  if (!isValid) throw new UnauthorizedError();

  return userExists.toJSON();
};
