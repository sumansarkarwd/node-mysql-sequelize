const sharp = require('sharp');
const logger = require('../utils/logger');

module.exports = async function genThumb(files) {
  if (!Array.isArray(files)) {
    return;
  }

  await Promise.all(
    files.map(async (file) => {
      const { thumbnail } = file;

      if (!Object.values(thumbnail)) {
        return;
      }

      await Promise.all(
        Object.keys(thumbnail).map(async (thumbDimension) => {
          const originalFilePath = file.path;

          try {
            await sharp(originalFilePath)
              .resize(parseInt(thumbDimension, 10))
              .toFile(`./src/public/uploads/${file.name.split('.')[0]}_${thumbDimension}.jpg`);
          } catch (error) {
            logger.error(error.message);
          }
        }),
      );
    }),
  );
};
