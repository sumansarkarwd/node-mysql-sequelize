const uploadController = require('../controllers/upload.controller');
const mapRoutes = require('../utils/mapRoutes');

const routes = {
  prefix: '/upload',
  routes: [
    {
      method: 'post',
      route: '',
      handler: uploadController.upload,
    },
  ],
};

module.exports = mapRoutes(routes);
