const chalk = require('chalk');
const { Sequelize, DataTypes } = require('sequelize');
const config = require('../utils/config');
const logger = require('../utils/logger');

const sequelize = new Sequelize(config.DB_NAME, config.DB_USER, config.DB_PASSWORD, {
  host: config.DB_HOST,
  port: config.DB_PORT,
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000,
  },
});

sequelize
  .authenticate()
  .then(() => {
    logger.info(chalk.green('DB Connection has been established successfully.'));
  })
  .catch((err) => {
    logger.error(`Unable to connect to the database: ${err?.message}`);
    process.exit(1);
  });

const users = require('./users.model')(sequelize, DataTypes);

const db = {
  sequelize,
  Sequelize,
  users,
};

db.sequelize.sync().then(() => {
  logger.info(chalk.green('DB Sync has been completed successfully.'));
});

module.exports = db;
