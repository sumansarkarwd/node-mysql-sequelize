const uploadController = require('../controllers/upload.controller.js');
const mapRoutes = require('../utils/mapRoutes.js');

const routes = {
  prefix: '/upload',
  routes: [
    {
      method: 'post',
      route: '',
      handler: uploadController.upload,
    },
  ],
};

module.exports = mapRoutes(routes);
