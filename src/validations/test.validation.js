const Joi = require('joi');
const JoiDate = require('@hapi/joi-date');
const { findId, password } = require('./custom.validation');

const joi = Joi.extend(JoiDate);

module.exports.validateTest = {
  params: joi.object().keys({
    id: joi.number().required().external(findId),
  }),
  body: joi.object().keys({
    password: joi.string().custom(password).required(),
    startDate: joi.date().format('YYYY-MM-DD').required(),
    endDate: joi.when('startDate', {
      is: joi.exist(),
      then: joi.date().format('YYYY-MM-DD').raw().required().min(joi.ref('startDate')),
    }),
  }),
};
