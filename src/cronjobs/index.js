const { CronJob } = require('cron');
const { Worker, SHARE_ENV } = require('worker_threads');
const logger = require('../utils/logger');

module.exports.dummyCron = new CronJob(
  '5 * * * *',
  () => {
    const worker = new Worker('./src/workers/dummy.worker.js', {
      env: SHARE_ENV,
    });
    worker.on('error', (err) => logger.error(err.message));
    worker.on('message', (msg) => logger.info(msg));
    worker.on('exit', (code) => logger.info(`Thread exited with code: ${code}`));
  },
  null,
  true,
  'America/Los_Angeles',
);
