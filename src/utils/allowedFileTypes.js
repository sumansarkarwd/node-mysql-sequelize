const mimeTypes = [
  'image/jpeg',
  'image/jpg',
  'image/png',
  'application/msword',
  'application/pdf',
  'video/mp4',
  'video/webm',
  'video/x-m4v',
  'video/quicktime',
];

module.exports = ({ mimetype }) => mimetype && mimeTypes.includes(mimetype);
