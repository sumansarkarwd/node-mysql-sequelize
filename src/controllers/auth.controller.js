const authService = require('../services/auth.service');
const tokenService = require('../services/token.service');
const SuccessResponse = require('../utils/successResponse');

/**
 * Register
 * @typedef {object} Register
 * @property {string} firstName.form.required
 * @property {string} lastName.form.required
 * @property {string} email.form.required
 * @property {string} password.form.required
 */
/**
 * POST /auth/register
 * @summary Auth
 * @tags Auth
 * @param {Register}  request.body.required
 * @returns {object} - 200 - success response
 * @example response - 200
{
  "data": {
    "user": {
      "id": 5,
      "firstName": "John",
      "lastName": "Doe",
      "email": "...user-email",
      "updatedAt": "2022-04-01T14:10:12.811Z",
      "createdAt": "2022-04-01T14:10:12.811Z"
    }
  },
  "message": "OK",
  "httpCode": 200
}
*/
module.exports.register = async (req) => {
  const user = await authService.register(req.body);

  return new SuccessResponse({ user });
};

/**
 * Login
 * @typedef {object} Login
 * @property {string} email.form.required
 * @property {string} password.form.required
 */
/**
 * POST /auth/login
 * @summary Auth
 * @tags Auth
 * @param {Login}  request.body.required
 * @returns {object} - 200 - success response
 * @example response - 200
{
  "data": {
    "user": {
      "id": 4,
      "firstName": "string",
      "lastName": "string",
      "email": "...user-email",
      "password": "...hashed-password",
      "createdAt": "2022-04-01T13:54:40.000Z",
      "updatedAt": "2022-04-01T13:54:40.000Z"
    },
    "tokens": {
      "access": {
        "token": "...token",
        "expires": "2022-05-01T14:27:29.796Z"
      },
      "refresh": {
        "token": "...token",
        "expires": "2022-05-01T14:27:29.796Z"
      }
    }
  },
  "message": "OK",
  "httpCode": 200
}
*/
module.exports.login = async (req) => {
  const user = await authService.login(req.body);

  const tokens = tokenService.genToken(user);

  return new SuccessResponse({ user, tokens });
};

/**
 * GET /auth/me
 * @summary Auth
 * @tags Auth
 * @security BearerAuth
 * @param {Login}  request.body.required
 * @returns {object} - 200 - success response
 * @example response - 200
{
  "data": {
    "user": {
      "id": 4,
      "firstName": "string",
      "lastName": "string",
      "email": "suman@codebuddy.co",
      "createdAt": "2022-04-01T13:54:40.000Z",
      "updatedAt": "2022-04-01T13:54:40.000Z"
    }
  },
  "message": "OK",
  "httpCode": 200
}
*/
module.exports.me = async (req) => {
  const { user } = req;
  return new SuccessResponse({ user });
};
