const testController = require('../controllers/test.controller');
const validate = require('../middlewares/validateData');
const mapRoutes = require('../utils/mapRoutes');
const { validateTest } = require('../validations/test.validation');

const routes = {
  prefix: '/',
  routes: [
    {
      method: 'get',
      route: 'test',
      handler: testController.test,
      middleware: [],
    },
    {
      method: 'post',
      route: 'test/:id',
      handler: testController.testPost,
      middleware: [validate(validateTest)],
    },
  ],
};

module.exports = mapRoutes(routes);
