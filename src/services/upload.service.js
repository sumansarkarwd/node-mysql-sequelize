const formidable = require('formidable');
const { unlinkSync } = require('fs');
const eventEmitter = require('../events/index');
const allowedFileTypes = require('../utils/allowedFileTypes');
const config = require('../utils/config');
const randomFileName = require('../utils/randomFileName');

const THUMBNAIL_DIMENSIONS = [100, 400];

module.exports.upload = async (req) => {
  const formidableOptions = {
    uploadDir: './src/public/uploads',
    keepExtensions: true,
    maxFileSize: 30 * 1024 * 1024,
    maxFields: 1,
    allowEmptyFiles: false,
    multiples: true,
    filename: randomFileName,
    filter: allowedFileTypes,
  };

  const form = new formidable.IncomingForm(formidableOptions);

  const filePaths = [];

  const files = [];

  form.parse(req);

  return new Promise((resolve, reject) => {
    form.on('fileBegin', (formName, file) => {
      filePaths.push(file.filepath);
    });

    form.once('error', (err) => {
      try {
        filePaths.forEach((i) => {
          unlinkSync(i);
        });
      } catch (error) {
        reject(err);
      }
      reject(err);
    });

    form.on('file', (formName, file) => {
      const absolutePath = `${config.API_HOST}/src/public/uploads/`;

      const thumbnail = {};

      THUMBNAIL_DIMENSIONS.forEach((i) => {
        thumbnail[i] = `${absolutePath + i + file.newFilename.split('.')[0]}.png`;
      });

      const obj = {
        name: file.newFilename,
        file: absolutePath + file.newFilename,
        // uploadedBy: req.user._id,
        mimetype: file.mimetype,
        size: file.size,
        thumbnail,
        path: `./src/public/uploads/${file.newFilename}`,
        duration: 30,
      };
      files.push(obj);
    });

    form.once('end', async () => {
      // TODO: save files to DB
      // const ids = await upload.bulkWrite(
      //   files.map((i) => ({ insertOne: { document: i } })),
      // );

      // TODO: gen thumbnail

      eventEmitter.emit('genThumbnail', files);

      // TODO: return file ids
      resolve(true);
    });
  });
};
