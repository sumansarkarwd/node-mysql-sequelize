const dotenv = require('dotenv');

dotenv.config();

module.exports = {
  APP_NAME: process.env.APP_NAME,
  NODE_ENV: process.env.NODE_ENV,
  PORT: process.env.PORT,
  API_HOST: process.env.API_HOST,
  SALT_ROUNDS: 10,
  JWT_SECRET: process.env.JWT_SECRET,
  JWT_EXPIRES_IN_MINUTES: 30 * 24 * 60, // 30 days
  DB_HOST: process.env.DB_HOST,
  DB_PORT: process.env.DB_PORT,
  DB_USER: process.env.DB_USER,
  DB_PASSWORD: process.env.DB_PASSWORD,
  DB_NAME: process.env.DB_NAME,
};
