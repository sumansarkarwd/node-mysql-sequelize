const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const passport = require('passport');
const { errorConverter, errorHandler, notFoundHandler } = require('./middlewares/errorHandling.middleware');
const routes = require('./routes/index');
const apiDocs = require('./utils/apiDocs');
const loadConfig = require('./utils/loadConfig');
const registerCronJobs = require('./cronjobs/registerCronJobs');
const { jwtStrategy } = require('./utils/loginStrategy');
require('./models');

loadConfig();

const app = express();

apiDocs(app);

app.use(cors());
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
app.use(bodyParser.json());

app.use(passport.initialize());
passport.use('jwt', jwtStrategy);

app.use('/', routes);

app.use(notFoundHandler);
app.use(errorConverter);
app.use(errorHandler);

registerCronJobs();

module.exports = app;
