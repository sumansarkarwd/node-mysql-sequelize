module.exports = (routes) =>
  routes.routes.map((i) => ({
    ...i,
    route: routes.prefix + i.route,
  }));
