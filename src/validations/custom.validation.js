module.exports.password = (value, helpers) => {
  if (value.length < 6) {
    return helpers.message('{{#label}} must be at least 6 characters');
  }
  if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
    return helpers.message('{{#label}} must contain at least 1 letter and 1 number');
  }
  return value;
};

module.exports.findId = (value) =>
  new Promise((res, reg) => {
    const values = [1, 2, 3];

    if (!values.includes(value)) {
      reg(new Error('Id not found'));
    } else {
      res(value);
    }
  });
