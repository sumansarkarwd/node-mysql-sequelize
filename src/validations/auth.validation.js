const Joi = require('joi');
const JoiDate = require('@hapi/joi-date');
const { password } = require('./custom.validation');

const joi = Joi.extend(JoiDate);

module.exports.register = {
  body: joi.object().keys({
    firstName: joi.string().required(),
    lastName: joi.string().required(),
    email: joi.string().email().required(),
    password: joi.string().custom(password).required(),
  }),
};

module.exports.login = {
  body: joi.object().keys({
    email: joi.string().email().required(),
    password: joi.string().required(),
  }),
};
