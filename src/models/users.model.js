const logger = require('../utils/logger');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'users',
    {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        unique: true,
      },
      password: DataTypes.STRING,
    },
    {
      timstamps: true,
    },
  );

  User.prototype.toJSON = function () {
    logger.info('called');
    const userObj = { ...this.dataValues };

    delete userObj.password;

    return userObj;
  };

  return User;
};
