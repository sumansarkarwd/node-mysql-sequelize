const expressJSDocSwagger = require('express-jsdoc-swagger');
const config = require('./config');

const { NODE_ENV, API_HOST } = config;

module.exports = (app) => {
  if (NODE_ENV === 'production') return;

  const options = {
    info: {
      version: '1.0.0',
      title: config.APP_NAME,
      license: {
        name: 'MIT',
      },
    },
    security: {
      BearerAuth: {
        type: 'http',
        scheme: 'bearer',
      },
    },
    filesPattern: '../**/*.js',
    swaggerUIPath: '/api-docs',
    baseDir: __dirname,
    exposeSwaggerUI: true, // Expose OpenAPI UI. Default true
    exposeApiDocs: false,
    notRequiredAsNullable: false,
    servers: [],
  };

  options.servers.push({
    url: API_HOST,
    description: 'Local development server',
  });

  expressJSDocSwagger(app)(options);
};
