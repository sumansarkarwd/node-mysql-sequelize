const ApiError = require('../utils/apiError');
const SuccessResponse = require('../utils/successResponse');
const uploadService = require('../services/upload.service');

/**
 * FileUpload
 * @typedef {object} FileUpload
 * @property {string} file - image - binary
 */
/**
 * POST /upload
 * @summary Universal file upload
 * @description image/jpeg, image/jpg, image/png, image/png file size is in bytes
 * @tags FileUpload
 * @param {FileUpload} request.body.required - binary file - multipart/form-data
 * @return {object} 200 - success response
 */
module.exports.upload = async (req) => {
  try {
    const data = await uploadService.upload(req);

    return new SuccessResponse({ ok: data });
  } catch (error) {
    throw new ApiError(error.message, 'UPLOAD_ERR');
  }
};
