const chalk = require('chalk');
const app = require('./app');
const config = require('./utils/config');
const logger = require('./utils/logger');

const server = app.listen(config.PORT, () => {
  logger.info(`Server started on ${chalk.blue.underline(config.API_HOST)}`);
  logger.info(`API docs: ${chalk.green.underline(`${config.API_HOST}/api-docs`)}`);
});

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.error('Server closed!');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  exitHandler();
});

process.on('SIGINT', () => {
  logger.info('SIGINT received');
  exitHandler();
});
