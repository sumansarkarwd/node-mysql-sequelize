const httpStatus = require('http-status');
const ApiError = require('../utils/apiError');
const { invalidLoginCredentials } = require('../utils/msgFactory');

module.exports = class UnauthorizedError extends ApiError {
  constructor() {
    super(invalidLoginCredentials(), 'UNAUTHORIZED', null, httpStatus.UNAUTHORIZED);
  }
};
