const passport = require('passport');
const UnauthorizedError = require('../errors/unauthorized.error');

const verifyCallback = (req, resolve, reject) => async (err, user, info) => {
  if (err || info || !user) return reject(new UnauthorizedError());
  req.user = user;
  resolve();
};

module.exports = () => async (req, res, next) =>
  new Promise((resolve, reject) => {
    passport.authenticate('jwt', { session: false }, verifyCallback(req, resolve, reject, module))(req, res, next);
  })
    .then(() => next())
    .catch((err) => next(err));
