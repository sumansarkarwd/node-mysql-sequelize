const winston = require('winston');

const DailyRotateFile = require('winston-daily-rotate-file');

const myFormat = winston.format.printf((data) => {
  const { level, message, timestamp, stack } = data;

  if (stack) {
    return `[${timestamp}] [${level}]: ${message} \n${JSON.stringify(data.stack, null, 2)}`;
  }

  return `[${timestamp}] [${level}]: ${message}`;
});

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD hh:mm:ss a' }),
    winston.format.colorize(),
    myFormat,
  ),
  transports: [],
});

/**
 * If we're not in production then log to the `console` with the format:
 * ${info.level}: ${info.message} JSON.stringify({ ...rest }) `
 */
if (process.env.NODE_ENV !== 'production') {
  logger.add(
    new winston.transports.Console({
      format: winston.format.combine(myFormat),
    }),
  );
}

logger.add(
  new DailyRotateFile({
    filename: 'logs/%DATE%-log.log',
    datePattern: 'YYYY-MM-DD',
    zippedArchive: true,
    maxSize: '5m',
    maxFiles: '60d',
    format: winston.format.uncolorize(),
  }),
);

module.exports = logger;
