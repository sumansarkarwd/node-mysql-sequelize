const jwt = require('jsonwebtoken');
const moment = require('moment-timezone');
const config = require('../utils/config');

module.exports.genToken = (user) => {
  const tokenExpires = moment().add(config.JWT_EXPIRES_IN_MINUTES, 'minutes');

  const accessToken = jwt.sign(
    {
      sub: user.id,
      iat: moment().unix(),
      exp: tokenExpires.unix(),
      type: 'access',
    },
    config.JWT_SECRET,
  );

  const refreshToken = jwt.sign(
    {
      sub: user.id,
      iat: moment().unix(),
      exp: tokenExpires.unix(),
      type: 'refresh',
    },
    config.JWT_SECRET,
  );

  return {
    access: {
      token: accessToken,
      expires: tokenExpires.toDate(),
    },
    refresh: {
      token: refreshToken,
      expires: tokenExpires.toDate(),
    },
  };
};
