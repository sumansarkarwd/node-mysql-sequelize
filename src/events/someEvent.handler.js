const logger = require('../utils/logger');

module.exports = (msg) => {
  logger.info(`SomeEvent handler: ${msg}`);
};
